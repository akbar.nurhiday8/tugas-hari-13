@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')
<h3>Sign Up Form</h3>
<form action="/welcome" method="POST">
    @csrf
    <label for="fname">First Name : </label><br><br>
    <input type="text" name="nama1"><br><br>
    <label for="lname">Last Name:</label><br><br>
    <input type="text" name="nama2"><br><br>

    <label for="gender">Gender:</label><br><br>
    <input type="radio" name="gender" value="Male" > <label for="male"> Male</label><br>
    <input type="radio" name="gender" value="Female" > <label for="female"> Female</label><br>
    <input type="radio" name="gender" value="Other"> <label for="other"> Other</label><br><br>

    <label for="nationality">Nationality:</label><br><br>
    <select name="national" id="national">
        <option value="Indonesia" >Indonesia</option>
        <option value="Malaysia" >Malaysia</option>
        <option value="Singapura" >Singapura</option>
        <option value="Thailand" >Thailand</option>
    </select><br><br>

    <label for="bahasa">Language Spoken:</label><br><br>
    <input type="checkbox"><label for="bhsind" name="bhs[]" value="Indonesia"> Bahasa Indonesia</label><br>
    <input type="checkbox"><label for="english"name ="bhs[]" value="English"> English</label><br>
    <input type="checkbox"><label for="bhsother" name="bhs[]" value="Other"> Other</label><br><br>

    <label for="bio">Bio:</label><br>
    <textarea name="bio1" id="bio1" cols="30" rows="10"></textarea><br>
    <input type="submit" value="Sign Up">
</form>

@endsection